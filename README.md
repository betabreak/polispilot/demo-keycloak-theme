# PolisPilot Demo KeyCloak Theme

This project contains a KeyCloak theme for login and registration. The demo
directory needs to be placed in the server's root themes directory. After that
the "demo" theme can be selected as a "Login theme" from the "Themes" tab in
the "Realm Settings".
